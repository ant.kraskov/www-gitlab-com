---
layout: handbook-page-toc
title: DevOps for Mobile Apps Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mission

Our goal is to improve the experience for Developers targeting mobile platforms by providing CI/CD capabilities and workflows that will enhance the experience of provisioning and deploying mobile apps to iOS and Android devices. Our current focus combines the [Product Direction - DevOps for Mobile Applications ](https://about.gitlab.com/direction/mobile/mobile-devops/) and findings from our recent research.

## Our Hypothesis

Mobile software teams can see the same benefits as other software teams when adopting DevOps practices, but mobile teams are underserved in this area for a variety of reasons.

We believe that mobile teams are looking to adopt DevOps practices, but due to unfamiliar tooling and technical complexity, these initiatives can get time-consuming, expensive, and may not reach their full potential.

Our hypothesis is that we can improve the adoption of DevOps practices by mobile teams by providing opinionated tooling that is easy to use and doesn't require in-depth knowledge of tools unfamiliar to mobile developers like Docker, YAML, etc.

## Our Focus

Our current focus is around making the `build` > `test` > `release` process as simple as possible for mobile teams using GitLab.

Integration with [Fastlane](https://fastlane.tools/) is important as it is a very popular automation tool in the community today and can help enable the simplicity we hope to achieve.

We’ve also focused on [Review Apps for Mobile](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/15), which aims to make adding Review Apps to an existing pipeline simple. We believe this can be a differentiator as few competitors have this capability.

## Competitive Landscape

There are several competitors in this space providing visual pipeline builders on top of their own CI/CD systems (Bitrise, Appcircle, Buddybuild, and Codemagic) and the more prominent players, including Visual Studio App Center, Firebase, and AWS Mobile Services.

## Who We Are

The DevOps for Mobile Apps SEG is a [Single-Engineer Group](https://about.gitlab.com/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](https://about.gitlab.com/handbook/engineering/incubation/).

## How We Work

As we explore the opportunities and challenges in this space, we will share weekly demos. These demos will be recorded and shared in the [Weekly Demos issue](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/7) (which can be subscribed to for notification updates) as well as in the table below:

| Date              | Video | Issue |
|-------------------|-------|-------|
| November 8, 2021 | [https://youtu.be/3LnIuxI68BI](https://youtu.be/3LnIuxI68BI) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/31](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/31) |
| November 1, 2021 | [https://youtu.be/O3LOSClDuiI](https://youtu.be/O3LOSClDuiI) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/30](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/30) |
| October 21, 2021 | [https://youtu.be/WcS4zMoeGxk](https://youtu.be/WcS4zMoeGxk) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/26](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/26) |
| October 13, 2021 | [https://youtu.be/xKwXNEDLFXQ](https://youtu.be/xKwXNEDLFXQ) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/25](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/25) |
| September 23, 2021 | [https://youtu.be/iq5GRlnXd4E](https://youtu.be/iq5GRlnXd4E) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/23](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/23) |
| September 17, 2021 | [https://youtu.be/2bej-XonbCQ](https://youtu.be/2bej-XonbCQ) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/18](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/18) |
| September 11, 2021 | [https://youtu.be/j15IU3d0fNg](https://youtu.be/j15IU3d0fNg) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/17](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/17) |
| September 3, 2021 | [https://youtu.be/V8rzqrWDpTw](https://youtu.be/V8rzqrWDpTw) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/16](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/16) |
| August 26, 2021   | [https://youtu.be/4Ijx4_KOBZI](https://youtu.be/4Ijx4_KOBZI) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/13](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/13) |
| August 19, 2021   | [https://youtu.be/96C9Xx7gzuM](https://youtu.be/96C9Xx7gzuM) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/12](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/12) |
| August 13, 2021   | [https://youtu.be/f4U_n-zJwYg](https://youtu.be/f4U_n-zJwYg) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/6](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/6) |

## How To Contribute

#### GitLab Issues

Please feel free to create issues or participate in discussions in our [issue board](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues).

#### Slack

We can also be found in Slack at `#incubation-eng` (GitLab Internal)
