---
layout: markdown_page
title: Handling trials for GitLab Self-Managed
category: GitLab Self-Managed licenses
description: Self-managed trials cannot be extended - a license must be re-issued and applied to the instance in order to "extend" a trial.
---

{:.no_toc}

----

Self-managed trials cannot be extended - a license must be re-issued and applied
to the instance in order to "extend" a trial.

To create a new trial license, follow the steps in the
[creating a license key workflow](creating_licenses.html).

> Note: Specific workflow steps and approvals to be added here.
